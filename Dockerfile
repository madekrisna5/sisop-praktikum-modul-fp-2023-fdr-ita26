# Menggunakan base image
FROM ubuntu:latest

# Menginstall dependensi
RUN apt-get update && apt-get install -y \
    gcc \
    git \
    make \
    netcat \
    vim

# Membuat direktori untuk kode sumber
RUN mkdir /src

# Menyalin kode sumber ke dalam direktori
COPY . /src

# Mengatur direktori kerja
WORKDIR /src

# Membangun program
RUN make

# Menentukan perintah untuk menjalankan aplikasi
CMD ["./start.sh"]