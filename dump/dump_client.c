#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BUFFER_SIZE 1024
#define PORT 12345

int main(int argc, char *argv[]) {
    if (argc != 4) {
        printf("Usage: ./%s -u [username] -p [password] [database_name]\n", argv[0]);
        return 1;
    }

    const char *username = argv[1];
    const char *password = argv[2];
    const char *database_name = argv[3];

    int client_socket;
    struct sockaddr_in server_address;

    char buffer[BUFFER_SIZE];

    client_socket = socket(PF_INET, SOCK_STREAM, 0);
    if (client_socket == -1) {
        perror("socket");
        return 1;
    }

    memset(&server_address, 0, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = inet_addr("127.0.0.1");
    server_address.sin_port = htons(PORT);

    if (connect(client_socket, (struct sockaddr *)&server_address, sizeof(server_address)) == -1) {
        perror("connect");
        return 1;
    }

    snprintf(buffer, BUFFER_SIZE, "DUMP %s %s %s", username, password, database_name);
    write(client_socket, buffer, strlen(buffer));

    while (1) {
        ssize_t bytes_received = read(client_socket, buffer, BUFFER_SIZE - 1);
        if (bytes_received < 1) {
            break;
        }

        buffer[bytes_received] = '\0';

        printf("%s", buffer);
    }

    close(client_socket);

    return 0;
}