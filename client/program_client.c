#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 4444

int main(int argc, char *argv[]) {
    int sockfd, ret;
    struct sockaddr_in serverAddr;

    int newSocket;
    struct sockaddr_in newAddr;

    socklen_t addr_size;
    char buffer[1024];

    char username[128];
    char password[128];

    if (argc != 5) {
        printf("Usage: %s -u [username] -p [password]\n", argv[0]);
        exit(1);
    }

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-u") == 0) {
            strcpy(username, argv[i+1]);
        } else if (strcmp(argv[i], "-p") == 0) {
            strcpy(password, argv[i+1]);
        }
    }

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("[-] Error in socket");
        exit(1);
    }
    printf("[+] Server socket created\n");

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(PORT);
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    ret = connect(sockfd, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
    if (ret < 0) {
        perror("[-] Error in connection");
        exit(1);
    }
    printf("[+] Connected to server\n");

    send(sockfd, username, strlen(username), 0);
    send(sockfd, password, strlen(password), 0);

    while (1) {
        printf(">>> ");
        fgets(buffer, 1024, stdin);
        send(sockfd, buffer, strlen(buffer), 0);

        if (strcmp(buffer, "exit\n") == 0) {
            close(sockfd);
            printf("[-] Disconnected from server\n");
            exit(1);
        }

        recv(sockfd, buffer, 1024, 0);
        printf("Server: %s", buffer);
    }

    return 0;
}