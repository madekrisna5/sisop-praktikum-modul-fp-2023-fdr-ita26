# Sisop-Praktikum-Modul-FP-2023-FDR-ITA26



## Penjelasan

### Client
- Program dimulai dengan mengimpor beberapa header yang diperlukan, seperti stdio.h, stdlib.h, string.h, unistd.h, sys/socket.h, sys/types.h, netinet/in.h, dan arpa/inet.h. Header ini menyediakan fungsi-fungsi dan struktur data yang dibutuhkan untuk operasi socket.

- Program mendefinisikan konstanta PORT dengan nilai 4444. Konstanta ini digunakan untuk menentukan nomor port yang akan digunakan saat menghubungkan ke server.

- Program memulai fungsi main. Variabel-variabel yang akan digunakan dalam program dideklarasikan, antara lain:

sockfd: file descriptor socket yang akan digunakan untuk koneksi ke server.
serverAddr: struktur data sockaddr_in yang berisi informasi tentang server, seperti alamat IP dan nomor port.
newSocket dan newAddr: digunakan untuk menerima koneksi dari server, meskipun tidak digunakan dalam program ini.
addr_size: digunakan untuk menyimpan ukuran dari struktur sockaddr_in.
buffer: array karakter untuk menampung data yang dikirimkan dan diterima dari server.
username dan password: array karakter untuk menyimpan username dan password yang diberikan saat menjalankan program.
Program melakukan pemeriksaan argumen baris perintah. Jika jumlah argumen tidak sama dengan 5 (termasuk nama program itu sendiri), maka program mencetak pesan penggunaan yang benar dan keluar dari program dengan exit(1).

- Jika jumlah argumen benar, program mengambil nilai username dan password dari argumen baris perintah dan menyimpannya dalam variabel username dan password menggunakan loop for.

- Program membuat socket menggunakan fungsi socket() dengan parameter AF_INET untuk domain socket (IPv4) dan SOCK_STREAM untuk jenis socket (TCP). Jika pemanggilan fungsi socket() mengembalikan nilai yang lebih rendah dari 0, artinya terjadi kesalahan, dan program mencetak pesan kesalahan menggunakan perror() dan keluar dari program.

- Program mencetak pesan bahwa socket server telah berhasil dibuat.

- Program mengisi struktur serverAddr dengan informasi server yang akan dituju. Di sini, alamat IP ditentukan sebagai "127.0.0.1" (localhost), dan nomor port diambil dari konstanta PORT dan dikonversi menggunakan fungsi htons().

- Program melakukan koneksi ke server menggunakan fungsi connect() dengan parameter sockfd (socket client), serverAddr (alamat server), dan sizeof(serverAddr) (ukuran dari struktur serverAddr). Jika pemanggilan fungsi connect() mengembalikan nilai yang lebih rendah dari 0, artinya terjadi kesalahan, dan program mencetak pesan kesalahan menggunakan perror() dan keluar dari program.

- Program mencetak pesan bahwa koneksi ke server telah berhasil.

- Program mengirimkan username dan password ke server menggunakan fungsi send(). Fungsi ini mengirimkan data dengan parameter pertama sebagai socket client (sockfd), parameter kedua adalah buffer yang berisi data (username dan password), parameter ketiga adalah panjang data yang dikirim, dan parameter keempat adalah tanda flag (0 dalam hal ini).

- Program memasuki loop while yang berjalan selama 1 (true).

- Di dalam loop while, program mencetak prompt ">>> " untuk mengindikasikan bahwa pengguna diharapkan memasukkan perintah.

- Program membaca perintah yang dimasukkan oleh pengguna menggunakan fungsi fgets(). Perintah tersebut disimpan dalam buffer buffer.

- Program mengirimkan perintah ke server menggunakan fungsi send(). Perintah yang dikirimkan adalah isi buffer buffer dengan panjangnya yang dikirim sebagai parameter ketiga.

- Program memeriksa apakah perintah yang dikirimkan adalah "exit\n". Jika ya, program menutup socket dengan close(sockfd), mencetak pesan bahwa koneksi telah terputus, dan keluar dari program dengan exit(1).

- Jika perintah yang dikirimkan bukan "exit\n", program menerima respons dari server menggunakan fungsi recv(). Respons tersebut disimpan dalam buffer buffer.

- Program mencetak pesan dari server.

- Loop while berlanjut ke langkah 13 dan berulang terus menerus sampai perintah "exit\n" diberikan oleh pengguna.

- Setelah loop selesai, program keluar dan fungsi main() mengembalikan nilai 0.

### Database
- Program dimulai dengan mengimpor beberapa header yang diperlukan, seperti stdio.h, stdlib.h, string.h, unistd.h, sys/types.h, sys/socket.h, netinet/in.h, arpa/inet.h, dan pthread.h. Header ini menyediakan fungsi-fungsi dan struktur data yang dibutuhkan untuk operasi socket dan thread.
 
- Program mendefinisikan konstanta PORT dengan nilai 8080 yang menentukan nomor port yang akan digunakan oleh server. Konstanta ini juga akan digunakan oleh client untuk menghubungkan ke server.
 
- Program mendefinisikan konstanta MAX_CLIENTS dengan nilai 5 yang menentukan jumlah maksimum client yang dapat terhubung secara simultan ke server.
 
- Program mendefinisikan prototipe fungsi connection_handler dan handle_command yang akan diimplementasikan nanti.
 
- Program memulai fungsi main. Variabel-variabel yang akan digunakan dalam program dideklarasikan, antara lain:
 
 server_sock dan client_sock: file descriptor untuk socket server dan socket client.
 server dan client: struktur data sockaddr_in yang berisi informasi tentang server dan client, seperti alamat IP dan nomor port.
 c: digunakan untuk menyimpan ukuran dari struktur sockaddr_in.
 thread_id: digunakan untuk menyimpan ID thread yang dibuat.
 client_message: array karakter untuk menyimpan pesan yang diterima dari client.
 Program membuat socket server menggunakan fungsi socket() dengan parameter AF_INET untuk domain socket (IPv4) dan SOCK_STREAM untuk jenis socket (TCP). Jika pemanggilan fungsi socket() mengembalikan nilai -1, artinya terjadi kesalahan, dan program mencetak pesan kesalahan menggunakan perror() dan keluar dari program.

- Program mengisi struktur server dengan informasi server, seperti alamat IP dan nomor port yang akan digunakan. Alamat IP diatur sebagai INADDR_ANY, yang berarti server akan mendengarkan semua alamat IP yang tersedia di host tersebut.

- Program melakukan binding socket server dengan alamat dan nomor port yang telah ditentukan menggunakan fungsi bind(). Jika pemanggilan fungsi bind() mengembalikan nilai kurang dari 0, artinya terjadi kesalahan, dan program mencetak pesan kesalahan menggunakan perror() dan keluar dari program.

- Program melakukan proses listening untuk koneksi dari client menggunakan fungsi listen() dengan parameter server_sock (socket server) dan MAX_CLIENTS (jumlah maksimum client yang diperbolehkan).

- Program mencetak pesan bahwa server sedang menunggu koneksi masuk.

- Program memulai loop while yang akan terus berjalan selama ada koneksi yang diterima dari client. Pada setiap iterasi loop, program menerima koneksi masuk dari client menggunakan fungsi accept(). Fungsi ini akan memblokir program sampai ada koneksi masuk. Jika ada koneksi masuk, fungsi accept() akan mengembalikan file descriptor untuk socket client baru.

- Program membuat thread baru untuk menangani koneksi dari client menggunakan fungsi pthread_create(). Thread ini akan menjalankan fungsi connection_handler dan menerima file descriptor socket client sebagai argumen. Jika pemanggilan fungsi pthread_create() mengembalikan nilai kurang dari 0, artinya terjadi kesalahan, dan program mencetak pesan kesalahan menggunakan perror() dan keluar dari program.

- Jika ada koneksi masuk yang gagal diterima, program mencetak pesan kesalahan menggunakan perror() dan keluar dari program.

- Setelah selesai menerima koneksi atau jika terjadi kesalahan, program kembali ke fungsi main().

- Fungsi connection_handler diimplementasikan sebagai fungsi yang akan dijalankan oleh setiap thread yang dibuat. Fungsi ini menerima file descriptor socket client sebagai argumen. Variabel-variabel yang akan digunakan dalam fungsi ini dideklarasikan, antara lain:

sock: file descriptor untuk socket client.
read_size: jumlah byte yang dibaca dari socket.
client_message: array karakter untuk menyimpan pesan yang diterima dari client.
Dalam loop while, fungsi ini membaca data dari socket client menggunakan fungsi recv() dan menyimpannya di client_message. Jumlah byte yang dibaca disimpan di read_size. Jika read_size lebih dari 0, artinya ada data yang berhasil dibaca.

- Fungsi handle_command dipanggil untuk menangani perintah yang diterima dari client. Fungsi ini belum diimplementasikan, dan perlu diisi dengan logika penanganan perintah yang sesuai dengan kebutuhan aplikasi.

- Setelah menangani perintah, buffer client_message di-set ke karakter null dan memset digunakan untuk mengosongkan buffer.

- Jika read_size adalah 0, artinya client telah memutuskan koneksi, dan fungsi mencetak pesan bahwa client telah terputus.

- Jika read_size adalah -1, artinya terjadi kesalahan dalam membaca data dari client, dan fungsi mencetak pesan kesalahan menggunakan perror().

- Fungsi connection_handler mengembalikan nilai 0.

- Fungsi handle_command diimplementasikan sebagai fungsi yang akan menangani perintah dari client. Namun, dalam kode yang diberikan, fungsi ini belum diisi dengan logika penanganan perintah yang sesuai dengan kebutuhan aplikasi. Perlu dilakukan implementasi tambahan untuk memproses perintah yang diterima dan memberikan respons kepada client.

- Fungsi main mengembalikan nilai 0 untuk menandakan bahwa program telah berakhir dengan sukses.


### Dump

- Program dimulai dengan mengimpor beberapa header yang diperlukan, seperti stdio.h, stdlib.h, string.h, unistd.h, arpa/inet.h, sys/socket.h, dan netinet/in.h. Header ini menyediakan fungsi-fungsi dan struktur data yang dibutuhkan untuk operasi socket dan I/O.
 
- Program mendefinisikan konstanta BUFFER_SIZE dengan nilai 1024 yang menentukan ukuran buffer untuk membaca dan menulis data.
 
- Program mendefinisikan konstanta PORT dengan nilai 12345 yang menentukan nomor port yang akan digunakan oleh server.
 
- Program memulai fungsi main. Program melakukan pemeriksaan jumlah argumen command line yang diberikan. Jika jumlah argumen tidak sama dengan 4, program mencetak pesan penggunaan yang benar dan keluar dengan kode error 1.
 
- Program mengambil nilai argumen command line yang diberikan, yaitu username, password, dan database_name, dan menyimpannya dalam variabel yang sesuai.
 
- Variabel-variabel yang akan digunakan dalam program dideklarasikan, antara lain:
 
 client_socket: file descriptor untuk socket client.
 server_address: struktur data sockaddr_in yang berisi informasi tentang server, seperti alamat IP dan nomor port.
 buffer: array karakter untuk menyimpan data yang akan ditulis atau diterima.
 Program membuat socket client menggunakan fungsi socket() dengan parameter PF_INET untuk domain socket (IPv4) dan SOCK_STREAM untuk jenis socket (TCP). Jika pemanggilan fungsi socket() mengembalikan nilai -1, artinya terjadi kesalahan, dan program mencetak pesan kesalahan menggunakan perror() dan keluar dari program.
 
- Mengosongkan struktur server_address menggunakan memset() dan mengisi beberapa field di dalam struktur tersebut. sin_family diatur sebagai AF_INET untuk menunjukkan penggunaan protokol IPv4. sin_addr.s_addr diatur sebagai inet_addr("127.0.0.1") untuk menetapkan alamat IP server. sin_port diatur sebagai htons(PORT) untuk menetapkan nomor port server dengan menggunakan byte order yang sesuai.
 
- Program melakukan koneksi ke server menggunakan fungsi connect() dengan parameter client_socket (socket client), server_address (alamat server), dan sizeof(server_address) (ukuran dari struktur server_address). Jika pemanggilan fungsi connect() mengembalikan nilai -1, artinya terjadi kesalahan, dan program mencetak pesan kesalahan menggunakan perror() dan keluar dari program.
 
- Program menggunakan snprintf() untuk mengisi buffer dengan perintah "DUMP [username] [password] [database_name]". Perintah ini akan dikirimkan ke server untuk melakukan dump database.
 
- Program menulis data di dalam buffer ke socket menggunakan fungsi write() dengan parameter client_socket (socket client), buffer (data yang akan ditulis), dan strlen(buffer) (panjang data yang akan ditulis). Jika pemanggilan fungsi write() mengembalikan nilai yang kurang dari 0, artinya terjadi kesalahan, dan program keluar dari loop.
 
- Program memulai loop while yang berjalan selama 1 (benar). Di dalam loop ini, program membaca data dari socket menggunakan fungsi read() dengan parameter client_socket (socket client), buffer (buffer untuk menyimpan data yang diterima), dan BUFFER_SIZE - 1 (ukuran buffer dikurangi 1). Jumlah byte yang diterima disimpan di dalam variabel bytes_received.
 
- Jika bytes_received kurang dari 1, artinya tidak ada data yang diterima atau terjadi kesalahan, dan program keluar dari loop.
 
- Program menambahkan karakter null ('\0') ke akhir data yang diterima di dalam buffer untuk mengakhiri string.
 
- Program mencetak data yang diterima ke layar menggunakan printf().
 
- Program kembali ke awal loop dan membaca data berikutnya dari socket.
 
- Setelah loop selesai, program menutup socket client menggunakan fungsi close() dengan parameter client_socket.

- Program mengembalikan nilai 0 untuk menandakan bahwa program telah berakhir dengan sukses.
